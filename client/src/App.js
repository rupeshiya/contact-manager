import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import Header from './components/layouts/Header';
import Home from './components/layouts/Home';
import AddContact from './components/contacts/AddContact';
import Contacts from './components/contacts/Contacts';
import UpdateContact from './components/contacts/UpdateContact';
import './App.css';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
        <header className="App-header">
          <Header />
            <Switch>
              <Route exact path="/"  component={Home} />
              <Route exact path="/add/contact" component={AddContact} />
              <Route exact path="/get/contacts" component={Contacts} />
              <Route exact path="/update/contact/:id" component={UpdateContact} />
          </Switch>
        </header>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
