import { GET_CONTACTS, GET_ERRORS, GET_CONTACT_BY_ID, DELETE_CONTACT_BY_ID, CONTACTS_LOADING } from './types';

const axios = require('axios');
const baseUrl = "http://localhost:5000";

// get all contacts
export const getContacts  = () => dispatch => {
  dispatch(setContactLoading());
  console.log('get contacts called');
  axios.get(`${baseUrl}/api/get/contacts`)
    .then((response)=>{
      console.log('response for get contacts ', response);
      dispatch({
        type: GET_CONTACTS,
        payload: response.data.response
      })
    })
    .catch((error)=>{
      console.log(error);
      dispatch({
        type: GET_ERRORS,
        payload: {}
      })
    })
}

// get contact by id 
export const getContactById = (id) => dispatch => {
  dispatch(setContactLoading());
  axios.get(`${baseUrl}/api/get/contact/${id}`)
    .then((response)=>{
      console.log('response from get contact by id ', response);
      dispatch({
        type: GET_CONTACT_BY_ID,
        payload: response.data.response
      })
    })
    .catch((error)=>{
      dispatch({
        type: GET_ERRORS,
        payload: {}
      })
    })
}
// delete contact by id 
export const deleteContactById = (id, history) => dispatch => {
  axios.delete(`${baseUrl}/api/delete/contact/${id}`)
    .then((response) =>{
      console.log('response from delete contacts by id ', response);
      history.push('/get/contacts');
    })
    .catch((error)=>{
      console.log(error);
      dispatch({
        type: GET_ERRORS,
        payload: error
      })
    })
}
// update contacts 
export const updateContact = (id, contactInfo, history) => dispatch => {
  console.log(id);
  console.log(contactInfo);
  axios.put(`${baseUrl}/api/update/contact/${id}`, contactInfo)
    .then((response) => {
      console.log('response after updating contact ', response);
      dispatch({
        type: GET_CONTACTS,
        payload: response.data
      })
      history.push('/get/contacts');
    })
    .catch((error)=>{
      console.log(error);
      dispatch({
        type: GET_ERRORS,
        payload: {}
      })
    })
}

// add contact
export const addContact = (contactInfo, history) => dispatch => {
  axios.post(`${baseUrl}/api/add/contact`, contactInfo)
    .then((response) => {
      console.log('response after add contact ', response.data);
      history.push('/get/contacts');
    })
    .catch((error) => {
      dispatch({
        type: GET_ERRORS,
        payload: error
      });
    });
}

export const setContactLoading = () => {
  return {
    type: CONTACTS_LOADING
  }
}