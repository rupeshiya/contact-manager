import { GET_CONTACTS, GET_CONTACT_BY_ID, UPDATE_CONTACT_BY_ID, GET_ERRORS } from '../actions/types';
const intialState = {
  contact: {},
  contacts: [],
  isContactloading: true,
  errors: null
};

export default function(state = intialState, action){
  switch(action.type){
    case GET_CONTACTS: 
      return {
        ...state,
        contacts: action.payload,
        isContactloading: false
      }
    case GET_CONTACT_BY_ID:
      return {
        ...state,
        contact: action.payload,
        isContactloading: false
      }
    case UPDATE_CONTACT_BY_ID:
      return {
        ...state,
        contact: action.payload
      }
    case GET_ERRORS:
      return {
        ...state,
        errors: action.payload
      }
    default:{
      return state
    }
  }
}