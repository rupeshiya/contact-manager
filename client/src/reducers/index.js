import { combineReducers } from 'redux';
import contactReducers from './contactReducers';

const rootReducer = combineReducers({
  contacts: contactReducers
});

export default rootReducer;