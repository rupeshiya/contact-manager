import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Darkmode from 'darkmode-js';

class Header extends Component {
  constructor(props){
    super(props);
    this.state = {
      queryHandle: ''
    }
    var options = {
      bottom: '64px', // default: '32px'
      right: '32px', // default: '32px'
      left: 'unset', // default: 'unset'
      time: '1.0s', // default: '0.3s'
      mixColor: '#fff', // default: '#fff'
      backgroundColor: '#fff', // default: '#fff'
      buttonColorDark: '#100f2c', // default: '#100f2c'
      buttonColorLight: '#fff', // default: '#fff'
      saveInCookies: false, // default: true,
      label: '🌓', // default: ''
      autoMatchOsTheme: true // default: true
    }
   new Darkmode(options).showWidget();
  }

 
  render() {
    // const isAuthenticated = this.props.auth.isAuthenticated;
    const isAuthenticated = false;
    // const user = this.props.auth.user;

    const onLogoutClick = (e) =>{
      e.preventDefault();
      console.log('logout clicked');
      // this.props.clearCurrentProfile();
      // this.props.logoutUser();
    }

    // links for authenticated users
    const authLinks = (
        <ul className="navbar-nav ml-auto">
           <li className="nav-item">
            <Link className="nav-link" to="/add/contact">Add Contact</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/dashboard">Dashboard</Link>
          </li>
          <li className="nav-item">
            <form className="form-inline my-2 my-lg-0">
              <input 
                className="form-control mr-sm-2" 
                type="search" 
                placeholder="Search" 
                aria-label="Search"
                onChange={this.onChange}
               />
            </form>
          </li>
          <li className="nav-item">
            {/* <a href="" onClick={onLogoutClick} className="nav-link" to="/">
            <img src={user.avatar} alt={user.name} title="You should have gravatar related to your mail" style={{ width: '25px', margingRight: '5px', borderRadius: '50%'}}/>Logout</a> */}
          </li>
        </ul>
    );
    //  links for guests
    const guestLinks = (
      <ul className="navbar-nav ml-auto">
          {/* <li className="nav-item">
            {new Darkmode.showWidget()}
          </li> */}
          <li className="nav-item">
            <Link className="nav-link" to="/add/contact">Add Contact</Link>
          </li>
          <li className="nav-item">
            {/* <Link className="nav-link" to="/login"></Link> */}
          </li>
        </ul>
    );

    return (
       <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4">
    <div className="container">
      <Link className="navbar-brand" to="/">Contact manager</Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-nav">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="mobile-nav">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/get/contacts"> Contacts
            </Link>
          </li>
        </ul>
        {isAuthenticated ? authLinks: guestLinks}
      </div>
    </div>
  </nav>
    )
  }
}
// validate props
Header.propTypes = {
  // auth: PropTypes.object.isRequired,
  contacts: PropTypes.object.isRequired
}

// to use isAuthenticated in this component mapStateToProps 
const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    contacts: state.contacts
  }
}


export default connect(mapStateToProps, null)(Header);