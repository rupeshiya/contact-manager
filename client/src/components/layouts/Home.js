import React, { Component } from 'react'

class Home extends Component {
  render() {
    return (
       <div>
        <div className="landing">
          <div className="dark-overlay landing-inner text-light">
            <div className="container">
              <div className="row">
                <div className="col-md-12 text-center mt-4">
                  <h1 className="display-3" style={{"marginTop": "10%"}}><span style={{"color": "#821d04", "fontWeight":"bold"}}>C</span>ontact <span style={{"color":"#821d04", "fontWeight":"bold"}}>M</span>anager
                  </h1>
                  <hr />
                  {/* { !isAuthenticated ? guestLink : null } */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default Home;