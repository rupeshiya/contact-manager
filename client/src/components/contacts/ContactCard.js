import React, { Component } from 'react';
import { deleteContactById } from '../../actions/contactAction';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

class ContactCard extends Component {
  constructor(props){
    super(props);
    this.state = {
      showDetails: false
    }
  }

showDetails = () => {
  console.log("show clicked ");
  this.setState({showDetails: !this.state.showDetails});
}

onDeleteClick = (id) => {
  console.log('delete clicked');
  this.props.deleteContactById(id, this.props.history);
}

onEditClick = (contactId) => {
  this.props.history.push(`/update/contact/${contactId}`);  
}
  render() {
    const { contact, contactId } = this.props;
    return (
      <div className="card mb-3 m-auto"  style={{"width": "60%"}}>
      <div className="card-header">
        {contact.name}
        <a href="javascript: void(0)" onClick={this.showDetails}><i className="fas fa-sort-down" style={{"float": "right", "cursor": "pointer" }}></i></a>
        <a href="javascript: void(0)" onClick={this.onDeleteClick.bind(this, contactId)}><i className="fas fa-trash-alt mr-3" style={{"float": "right", "cursor": "pointer" }}></i></a>
        <a href="javascript: void(0)" onClick={this.onEditClick.bind(this, contactId)}><i className = "fas fa-pencil-alt mr-3" style={{"float": "right", "cursor": "pointer"}}></i></a>
      </div>
        {Boolean(this.state.showDetails) ? 
        (<div className = "card-body" >
          <blockquote className="blockquote mb-0">
          <p className="text text-muted">Email: {contact.email}</p>
          <p className="text text-muted">Phone: {contact.phone}</p>
          <p className="text text-muted">Address: {contact.address} </p>
          </blockquote>
        </div>) : null
        }
      </div>
    )
  }
}
// validat props 
ContactCard.propTypes = {
  contact: PropTypes.object.isRequired,
  contactId: PropTypes.number.isRequired
}

export default connect(null, { deleteContactById })(withRouter(ContactCard));