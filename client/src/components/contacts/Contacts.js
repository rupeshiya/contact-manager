import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getContacts } from '../../actions/contactAction';
import ContactCard from './ContactCard';
import PropTypes from 'prop-types';

class Contacts extends Component {
  constructor(props){
    super(props);
    this.state = {
      contacts: []      
    }
  }
  // fetch the data 
  componentDidMount() {
    this.props.getContacts();
  }

  // component will recieve props
  componentWillReceiveProps(nextProps){
    if(nextProps.contacts.contacts){
      console.log(nextProps.contacts.contacts);
      this.setState({contacts: nextProps.contacts.contacts});
    }
  }

  render() {
    const contacts = this.state.contacts;
    return (
      <div className="container">
        {contacts.map((contact) => (
          <ContactCard contact={contact} key={contact.id} contactId={contact.id}/>
        ))}
      </div>
     )
  }
}
// validate props 
Contacts.propTypes = {
  contacts : PropTypes.object.isRequired,
  getContacts: PropTypes.func.isRequired
}

// map state to props 
const mapStatetoProps = (state) => {
  return {
    contacts: state.contacts
  }
}
export default connect(mapStatetoProps, { getContacts })(Contacts);