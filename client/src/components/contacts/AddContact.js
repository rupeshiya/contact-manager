import React, { Component } from 'react'
import TextFieldGroup from '../../common/TextFieldGroup';
import TextAreaField from '../../common/TextAreaField';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { addContact } from '../../actions/contactAction';

class Addcontact extends Component {
  constructor(props){
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      address: ''
    }
  }
  onChange = (e) => {
    e.preventDefault();
    this.setState({[e.target.name]: e.target.value });
  }

  onSubmit = (e) => {
    e.preventDefault();
    console.log("form submitted");
    const contactsInfo = {
      name: this.state.name,
      email: this.state.email,
      address: this.state.address,
      phone: this.state.phone
    };
    this.props.addContact(contactsInfo, this.props.history);
  }
  
  render() {
    return (
      <div className="container">
        <div className="form m-auto" style={{"width": "60%"}}>
          <h1 className="text text-center">Add contact</h1>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
            <TextFieldGroup
              type="text"
              name="name"
              className="form-control"
              value={this.state.name}
              onChange={this.onChange}
              placeholder="Enter name"
            />
            <TextFieldGroup 
              name="email"
              placeholder="example@example.com"
              value={this.state.email}
              className="form-control"
              onChange={this.onChange}
              type="email"
              info="We will not share your email with anyone"
              required
            />
            <TextFieldGroup 
              type="phone"
              name="phone"
              placeholder="Phone number"
              className="form-control"
              value={this.state.phone}
              onChange={this.onChange}
              required
            />
            <TextAreaField 
             type="text"
             name="address"
             className="form-control"
             placeholder="Enter address"
             value={this.state.address}
             onChange={this.onChange}
             rows="3"
             required
            />
          </div>
           <button className="btn btn-block btn-outline-info" type="submit">Submit</button>
          </form>
        </div>
      </div>
    )
  }
}
// map state to props 
const mapStateToProps = (state) => {
  return {
    contacts: state.contacts
  }
}
export default connect(mapStateToProps, { addContact })((withRouter(Addcontact)));