import React, { Component } from 'react';
import TextFieldGroup from '../../common/TextFieldGroup';
import TextAreaField from '../../common/TextAreaField';
import { updateContact, getContactById } from '../../actions/contactAction';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

class UpdateContact extends Component {
  constructor(props){
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      address: ''
    }
  }

  // fetch the data 
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.getContactById(id);
  }

  componentWillReceiveProps(nextProps){
    console.log(nextProps);
    if(nextProps.contacts.contact){
      const { contact } = nextProps.contacts;
      this.setState({
        name: contact[0].name,
        email: contact[0].email,
        phone: contact[0].phone,
        address: contact[0].address
      });
    }
  }
  onChange = (e) => {
    e.preventDefault();
    this.setState({[e.target.name]: e.target.value });
  }

  onSubmit = (e) => {
    e.preventDefault();
    console.log("form submitted");
     const cotactInfo = {
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      address: this.state.address
    };
    const id = this.props.match.params.id;
    this.props.updateContact(id, cotactInfo, this.props.history);
  }
  
  render() {
    const { contact } = this.props.contacts;
    console.log(contact);

    return (
     <div className="container">
        <div className="form m-auto" style={{"width": "60%"}}>
          <h1 className="text text-center">Update contact</h1>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
            <TextFieldGroup
              type="text"
              name="name"
              className="form-control"
              value={this.state.name}
              onChange={this.onChange}
              placeholder="Enter name"
            />
            <TextFieldGroup 
              name="email"
              placeholder="example@example.com"
              value={this.state.email}
              className="form-control"
              onChange={this.onChange}
              type="email"
              info="We will not share your email with anyone"
              required
            />
            <TextFieldGroup 
              type="phone"
              name="phone"
              placeholder="Phone number"
              className="form-control"
              value={this.state.phone}
              onChange={this.onChange}
              required
            />
            <TextAreaField 
             type="text"
             name="address"
             className="form-control"
             placeholder="Enter address"
             value={this.state.address}
             onChange={this.onChange}
             rows="3"
             required
            />
          </div>
           <button className="btn btn-block btn-outline-info" type="submit">Submit</button>
          </form>
        </div>
      </div>
    )
  }
}
// validate props 
UpdateContact.propType = {
  contacts: PropTypes.object.isRequired,
  updateContact: PropTypes.func.isRequired,
  getContactById: PropTypes.func.isRequired
}

// map state to props 
const mapStateToProps = (state) => {
  return { 
    contacts: state.contacts
  }
}
export default connect(mapStateToProps, { updateContact, getContactById })((withRouter(UpdateContact)));