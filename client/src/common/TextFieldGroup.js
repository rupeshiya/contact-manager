import React from 'react'

const TextFieldGroup = ({
   placeholder, name, value, onChange, type, className, disabled, info, error, required
}) => {
  return (
    <div className="form-group">
      <input 
        type={type}
        placeholder={placeholder}
        disabled={disabled}
        name={name} 
        onChange={onChange} 
        required={required}
        value={value}
        className={className}
        />
        {info && <small className="form-text text-muted">{info}</small>}
        {error && <small className="form-text text-muted">{error}</small>}
    </div>
  )
}
TextFieldGroup.defaultProps = {
  type: 'text'
};
export default TextFieldGroup;