import React from 'react'

const TextAreaField = ({
  type, name, rows, value, className, required, info, onChange, placeholder, error
}) => {
  return (
    <div className="form-group">
      <textarea 
       type={type} 
       name={name} 
       placeholder={placeholder} 
       value={value} 
       onChange={onChange} 
       rows={rows} 
       className={className}
       />
      {info && <small className="form-text text-muted">{info}</small>}
      {error && <small className="form-text text-muted">{error}</small>}
    </div>
  )
}
export default TextAreaField;