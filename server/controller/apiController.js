const express = require('express');
const router = express.Router();
const connection = require('../db/connect');
const bcrypt = require('bcryptjs');

// routes
module.exports = {
  getContacts: function(req, res) {
    connection.query('SELECT * FROM contactInfo', (err, response) => {
      if (err) {
        console.log(err);
      } else {
        res.json({
          success: true,
          response
        });
      }
    })
  },
  addContact: function(req, res){
     const contactInfo = {
       name: req.body.name,
       phone: req.body.phone,
       address: req.body.address,
       email: req.body.email
     };
     connection.query("INSERT INTO contactInfo SET ?", (contactInfo), (err, response) => {
       if (err) {
         console.log(err);
       } else {
         console.log(response);
         res.json({
           success: true,
           response
         });
       }
     });
  },

  deleteContact: function(req, res){
      // check if id exist
      connection.query("SELECT * FROM contactInfo WHERE id = ?", (req.params.id), (err, response) => {
        if (err) {
          console.log(err);
          return res.status(404).json({
            success: false,
            msg: `${req.params.id} not found`
          });
        } else {
          connection.query("DELETE FROM contactInfo WHERE id = ?", (req.params.id), (err, response) => {
            if (err) {
              console.log(err);
            } else {
              console.log(response);
              res.json({
                success: true,
                response
              });
            }
          });
        }
      })
  },
  updateContact: function (req, res) {
    // check if exists
    connection.query("SELECT * FROM contactInfo WHERE id = ?", (req.params.id), (err, response) => {
      if (err) {
        console.log(err);
        return res.status(404).json({
          success: false,
          msg: `${req.params.id} not found`
        });
      } else {
        const info = [req.body.name, req.body.email, req.body.address, req.body.phone, req.params.id]
        connection.query("UPDATE contactInfo SET name = ?, email = ?, address = ?, phone = ? WHERE id = ? ", (info), (err, response) => {
          if (err) {
            console.log(err);
          } else {
            res.json({
              success: true,
              response
            });
          }
        });
      }
    });
  },

  getContactById: function(req, res) {
    connection.query("SELECT * FROM contactInfo WHERE id = ?", (req.params.id), (err, response) => {
      if (err) {
        console.log(err);
        return res.status(404).json({
          success: false,
          msg: `${req.params.id} not found `
        });
      } else {
        if (response.length > 0) {
          res.json({
            success: true,
            response
          });
        } else {
          res.status(400).json({
            success: false,
            msg: `${req.params.id} not found`
          });
        }
      }
    })
  }

}







