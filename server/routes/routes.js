const express = require('express');
const router = express.Router();
const connection = require('../db/connect');
const apiController = require('../controller/apiController');

// get contact
router.get('/get/contacts', apiController.getContacts);
// add contacts 
router.post('/add/contact',apiController.addContact);
// delete contacts
router.delete('/delete/contact/:id',apiController.deleteContact);
// update contact
router.put('/update/contact/:id', apiController.updateContact);
// get contact by id
router.get('/get/contact/:id', apiController.getContactById);


module.exports = router;