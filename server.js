const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');
const app = express();
const PORT = process.env.PORT || 5000;
const routes = require('./server/routes/routes');
const path = require('path');

// middlewares 
app.use(cors());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, '/client/build')));

// routes 
app.get('/',(req, res)=>{
  res.sendFile(path.join(__dirname, '/client/build/index.html'));
});
// using api 
app.use('/api', routes);

// listen
app.listen(PORT, () =>{
  console.log(`listening on ${PORT}`);
});
